import smpplib.client, smpplib.consts, smpplib.gsm
import logging
import sys

# if you want to know what's happening
logging.basicConfig(level='DEBUG')

# SMSC Connection Details
SMSC_IP = "smscsim.melroselabs.com"
SMSC_PORT = 2775
SYSTEM_ID = "860734"
SYSTEM_PASS = "6c953f"
SENDER_ID = "00860734"
RECEIVER_NO = "99860734"
MESSAGE = "Test poruka broj 1."

# Converting message into parts
parts, encoding_flag, msg_type_flag = smpplib.gsm.make_parts(MESSAGE)

# Connect to the remote SMSC gateway
client = smpplib.client.Client(SMSC_IP, SMSC_PORT)

# Print when obtain message_id
client.set_message_sent_handler(
    lambda pdu: sys.stdout.write('Poslata {} {}\n'.format(pdu.sequence, pdu.message_id)))
client.set_message_received_handler(
    lambda pdu: sys.stdout.write('Isporucena {}\n'.format(pdu.receipted_message_id)))

client.connect()

# Bind to the SMSC with the credentail
client.bind_transceiver(system_id=SYSTEM_ID, password=SYSTEM_PASS)

# Getting the message from parts and sending it
for part in parts:
    pdu = client.send_message(
        source_addr_ton=smpplib.consts.SMPP_TON_INTL,
        source_addr=SENDER_ID,
        dest_addr_ton=smpplib.consts.SMPP_TON_INTL,
        destination_addr=RECEIVER_NO,
        short_message=part,

        data_coding=encoding_flag,
        esm_class=msg_type_flag,
        registered_delivery=True,
    )
    print("Tekst poruke je: {}", pdu.short_message)
    print("Dužina poruke je: {}", pdu.sm_length)

client.listen()
